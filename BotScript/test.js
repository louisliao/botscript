const Web3 = require('web3');
const ethTx = require('ethereumjs-tx');
const keythereum = require("keythereum");
const io = require('socket.io-client');
const axios = require('axios');

import * as order from './action/order';
import * as erctoken from './action/erctoken';
import * as tx from './action/transaction';

/* token info */
let config = require('./data/config.js');
let tokenInfo = require('./data/tokenInfo.js');

/* connect to ethereum node */
const ethereumUri = 'https://betarpc.baliv.cc/ethrpc';
let web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(ethereumUri));

if(!web3.isConnected()) {
    throw new Error('unable to connect to ethereum node at ' + ethereumUri);
}else {
    console.log('connected successed');
    console.log('connected to ethereum node at ' + ethereumUri);
}

/* 初始化變數 */
let etherUnit = 10 ** 18;

function autoTrade(fromTokenName, toTokenName, price, amount) {
    // 四捨五入到小數第二位
    console.log("Spent "+ fromTokenName +" for: "+ (amount / etherUnit) + ",Price: "+ (price / etherUnit));
    let balance = erctoken.getTokenBalance(fromTokenName);
    
    if(amount < balance ** (10 ** 18)) {
        console.log(fromTokenName + " is enough, balace: " + balance);
        order.userTakeOrder(fromTokenName, toTokenName, price , amount, 0);
    }else {
        console.log(amount + ", " + balance); 
        console.log(fromTokenName + " is not enough! balace: " +  balance);
    }
}

let argvs = process.argv.slice(2);
let parameter = {
    fromToken: argvs[1],
    toToken: argvs[2],
    price: argvs[3],
    amount: argvs[4],
}

if(config.account == "") {
    console.log("please setting bot account and private key");
}else {
    if(argvs[1] == "approve0") {
        console.log("do approve 0");
        // setTimeout to waiting for web3.js loading in...
        setTimeout(function(){
            let token = argvs[2];
            order.approve(0, token).then(function(approveTx){
                console.log("do approve...");
                console.log(approveTx);
                return tx.sendTransaction(approveTx);
            }).then(function(approveHash) {
                console.log("approve hash");
                console.log(approveHash);
                return 0;
            }).catch(err => {
                console.log("error happen, infomation: ", err);
            });
        },3000);
    }else {
        setTimeout(function() {
            autoTrade(parameter.fromToken, parameter.toToken, parameter.price, parameter.amount);
        },3000);
    }
}

export default web3;
