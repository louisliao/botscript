var bankAbi = [
    {
      "constant": false,
      "inputs": [
        {
          "name": "bank_",
          "type": "address"
        }
      ],
      "name": "setBank",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "usable",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "makeFeeRate_",
          "type": "uint256"
        }
      ],
      "name": "setMakeFeeRate",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "fromToken_",
          "type": "address"
        },
        {
          "name": "fromAmount_",
          "type": "uint256"
        },
        {
          "name": "toToken_",
          "type": "address"
        },
        {
          "name": "toAmount_",
          "type": "uint256"
        }
      ],
      "name": "userMakeOrder",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "timeout",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "make_",
          "type": "uint256"
        },
        {
          "name": "take_",
          "type": "uint256"
        }
      ],
      "name": "operatorMakeTrade",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "name": "tradeBooks",
      "outputs": [
        {
          "name": "fromToken",
          "type": "address"
        },
        {
          "name": "fromAmount",
          "type": "uint256"
        },
        {
          "name": "toToken",
          "type": "address"
        },
        {
          "name": "toAmount",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "user_",
          "type": "address"
        }
      ],
      "name": "assignOperator",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "user_",
          "type": "address"
        }
      ],
      "name": "dismissOperator",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "name": "orderBooks",
      "outputs": [
        {
          "name": "no",
          "type": "uint256"
        },
        {
          "name": "user",
          "type": "address"
        },
        {
          "name": "fromToken",
          "type": "address"
        },
        {
          "name": "fromAmount",
          "type": "uint256"
        },
        {
          "name": "toToken",
          "type": "address"
        },
        {
          "name": "toAmount",
          "type": "uint256"
        },
        {
          "name": "filled",
          "type": "uint256"
        },
        {
          "name": "expire",
          "type": "uint256"
        },
        {
          "name": "orderType",
          "type": "uint256"
        },
        {
          "name": "status",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "unlock",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "fromToken_",
          "type": "address"
        },
        {
          "name": "toToken_",
          "type": "address"
        }
      ],
      "name": "getPrice",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "timeout_",
          "type": "uint256"
        }
      ],
      "name": "setTimeout",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "fromToken_",
          "type": "address"
        },
        {
          "name": "fromAmount_",
          "type": "uint256"
        },
        {
          "name": "toToken_",
          "type": "address"
        },
        {
          "name": "toAmount_",
          "type": "uint256"
        },
        {
          "name": "txnos_",
          "type": "uint256[]"
        }
      ],
      "name": "userTakeOrder",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "no_",
          "type": "uint256"
        }
      ],
      "name": "userCancelOrder",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "make_",
          "type": "uint256"
        },
        {
          "name": "take_",
          "type": "uint256"
        }
      ],
      "name": "makeTrade",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "user_",
          "type": "address"
        }
      ],
      "name": "checkOperator",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "no_",
          "type": "uint256"
        },
        {
          "name": "user_",
          "type": "address"
        },
        {
          "name": "fromToken_",
          "type": "address"
        },
        {
          "name": "fromAmount_",
          "type": "uint256"
        },
        {
          "name": "toToken_",
          "type": "address"
        },
        {
          "name": "toAmount_",
          "type": "uint256"
        },
        {
          "name": "filled_",
          "type": "uint256"
        },
        {
          "name": "expire_",
          "type": "uint256"
        },
        {
          "name": "orderType_",
          "type": "uint256"
        },
        {
          "name": "status_",
          "type": "uint256"
        }
      ],
      "name": "importOrder",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "name": "operators",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "newOwner_",
          "type": "address"
        }
      ],
      "name": "transferOwnership",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "takeFeeRate_",
          "type": "uint256"
        }
      ],
      "name": "setTakeFeeRate",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "lock",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "name": "operator_",
          "type": "address"
        },
        {
          "name": "bank_",
          "type": "address"
        },
        {
          "name": "ledger_",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "no",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "user",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "fromToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "fromAmount",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "toToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "toAmount",
          "type": "uint256"
        }
      ],
      "name": "MakeOrder",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "no",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "user",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "fromToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "fromAmount",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "toToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "toAmount",
          "type": "uint256"
        }
      ],
      "name": "TakeOrder",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "no",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "filled",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "status",
          "type": "uint256"
        }
      ],
      "name": "UpdateOrder",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "fromToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "fromAmount",
          "type": "uint256"
        },
        {
          "indexed": false,
          "name": "toToken",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "toAmount",
          "type": "uint256"
        }
      ],
      "name": "MakeTrade",
      "type": "event"
    }
];

module.exports = bankAbi;