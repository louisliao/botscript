var balivAbi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "bank_",
				"type": "address"
			}
		],
		"name": "setBank",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "usable",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "ledger_",
				"type": "address"
			}
		],
		"name": "setLedger",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "make_",
				"type": "uint256"
			},
			{
				"name": "take_",
				"type": "uint256"
			}
		],
		"name": "testPair",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "makeFeeRate_",
				"type": "uint256"
			}
		],
		"name": "setMakeFeeRate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "fromToken_",
				"type": "address"
			},
			{
				"name": "fromAmount_",
				"type": "uint256"
			},
			{
				"name": "toToken_",
				"type": "address"
			},
			{
				"name": "toAmount_",
				"type": "uint256"
			}
		],
		"name": "userMakeOrder",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "maxAmount_",
				"type": "uint256"
			}
		],
		"name": "setMaxAmount",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "takeFeeRate",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "timeout",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "user_",
				"type": "address"
			}
		],
		"name": "assignOperator",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "user_",
				"type": "address"
			}
		],
		"name": "dismissOperator",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "minAmount_",
				"type": "uint256"
			}
		],
		"name": "setMinAmount",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "unlock",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "fromToken_",
				"type": "address"
			},
			{
				"name": "toToken_",
				"type": "address"
			},
			{
				"name": "count_",
				"type": "uint256"
			}
		],
		"name": "getPrice",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "f1",
				"type": "uint256"
			},
			{
				"name": "t1",
				"type": "uint256"
			},
			{
				"name": "f2",
				"type": "uint256"
			},
			{
				"name": "t2",
				"type": "uint256"
			}
		],
		"name": "checkRatio",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "autoMatch",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "makeFeeRate",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "timeout_",
				"type": "uint256"
			}
		],
		"name": "setTimeout",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "fromToken_",
				"type": "address"
			},
			{
				"name": "fromAmount_",
				"type": "uint256"
			},
			{
				"name": "toToken_",
				"type": "address"
			},
			{
				"name": "toAmount_",
				"type": "uint256"
			}
		],
		"name": "userAutoOrder",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "fromToken_",
				"type": "address"
			},
			{
				"name": "fromAmount_",
				"type": "uint256"
			},
			{
				"name": "toToken_",
				"type": "address"
			},
			{
				"name": "toAmount_",
				"type": "uint256"
			},
			{
				"name": "txnos_",
				"type": "uint256[]"
			}
		],
		"name": "userTakeOrder",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "no_",
				"type": "uint256"
			}
		],
		"name": "getOrder",
		"outputs": [
			{
				"components": [
					{
						"name": "user",
						"type": "address"
					},
					{
						"name": "fromToken",
						"type": "address"
					},
					{
						"name": "fromAmount",
						"type": "uint256"
					},
					{
						"name": "toToken",
						"type": "address"
					},
					{
						"name": "toAmount",
						"type": "uint256"
					},
					{
						"name": "filled",
						"type": "uint256"
					},
					{
						"name": "expire",
						"type": "uint256"
					},
					{
						"name": "status",
						"type": "uint256"
					}
				],
				"name": "",
				"type": "tuple"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "no_",
				"type": "uint256"
			}
		],
		"name": "userCancelOrder",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "user_",
				"type": "address"
			}
		],
		"name": "checkOperator",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "make_",
				"type": "uint256"
			},
			{
				"name": "take_",
				"type": "uint256"
			}
		],
		"name": "trade",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "operators",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "counts_",
				"type": "uint256"
			}
		],
		"name": "setAutoMatch",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "newOwner_",
				"type": "address"
			}
		],
		"name": "transferOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "takeFeeRate_",
				"type": "uint256"
			}
		],
		"name": "setTakeFeeRate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "lock",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"name": "operator_",
				"type": "address"
			},
			{
				"name": "bank_",
				"type": "address"
			},
			{
				"name": "ledger_",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "",
				"type": "uint256"
			}
		],
		"name": "Trade",
		"type": "event"
	}
]

module.exports = balivAbi;