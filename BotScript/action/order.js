import web3 from '../test';
const ethTx = require('ethereumjs-tx');
let tx = require('./transaction.js');
let erc20 = require('./erctoken.js')
let config = require('../data/config.js');
let contractAddr = require('../data/contract.js');
let tokenInfo = require('../data/tokenInfo.js')

export function approve(amount_, token) {
    return new Promise(function(resolve, reject){
      let spender_ = contractAddr.Baliv.slice(2,);
      let tokenData = '0x';
      let amount = web3.toHex(amount_);
      let tokenAction = web3.sha3('approve(address,uint256)').slice(2,10);
      let tokenParams = tx.to64(spender_) + tx.to64(amount.slice(2,));
      tokenData += tokenAction + tokenParams;
      let tokenNonce = web3.eth.getTransactionCount(config.account);
      let gasPrice = web3.eth.gasPrice;
      console.log(token);
      console.log(erc20.checkTokenAddr(token));

      let txInfo = {
          nonce: web3.toHex(tokenNonce),
          gasPrice: web3.toHex(gasPrice),
          gasLimit: web3.toHex(1000000),
          to: "0x" + erc20.checkTokenAddr(token),
          value: '0x0',
          data: tokenData,
          chainId: 1
      }
      resolve(txInfo);
      reject('approve promise failed');
  });
}

export function userTakeOrder(fromToken_, toToken_, price_, amount_, representor_) {
    let block = web3.eth.getBlock("latest");
    let data = '0x';
    let method = web3.sha3('userTakeOrder(address,address,uint256,uint256,address)').slice(2,10);
    let price = web3.toHex(price_);
    let amount = web3.toHex(amount_);
    let representor = web3.toHex(representor_);
    let params = tx.to64(erc20.checkTokenAddr(fromToken_)) + tx.to64(erc20.checkTokenAddr(toToken_)) + tx.to64(price.slice(2,)) + tx.to64(amount.slice(2,)) + tx.to64(representor.slice(2,));
    data += method + params;
    let gasPrice = web3.eth.gasPrice;
    // var gasLimit = web3.eth.estimateGas({
    //     to: Baliv,
    //     data: data
    // });
    let gasLimit = 1000000;
    let value_ = 0x0;
    let nounce = web3.eth.getTransactionCount(config.account);
    let txInfo = {
        // from: config.account,
        nonce: web3.toHex(nounce),
        gasPrice: web3.toHex(gasPrice),
        gasLimit: web3.toHex(gasLimit),
        to: contractAddr.Baliv,
        value: value_,
        data: data,
        chainId: 1
    }
    console.log('====From '+fromToken_ +'====');
    if(fromToken_ == "ETH") {
        txInfo.value = amount;
        tx.sendTransaction(txInfo).then(function(txhash) {
            console.log('====userTakeOrder hash====');
            console.log(txhash);
        });
    }else{
        approve(amount_, fromToken_).then(function(approveTx){
            console.log("do approve...");
            console.log(approveTx);
            return tx.sendTransaction(approveTx);
        }).then(function(approveHash){
            console.log("approve hash");
            console.log(approveHash);
            return tx.getTransactionReceipt(approveHash);
        }).then(function(result){
            if(result) {
                // return non heximal format.
                let newNonce= web3.eth.getTransactionCount(config.account);
                txInfo.nonce = web3.toHex(newNonce);
                console.log(txInfo);
                return tx.sendTransaction(txInfo);
            }else {
                console.log('transaction failed');
            }
        }).then(function(txhash) {
            console.log('====userTakeOrder with approve hash====');
            console.log(txhash);
        }).catch(err => {
            console.log("caught: ", err);
        });
    }
}
