
import web3 from '../test.js';
let config = require('../data/config.js');
let tokenInfo = require('../data/tokenInfo.js');
let XPAabi = require('../abi/XPA.js');

export function getTokenBalance(token) {
    let contractAbi;
    let tokenAddress;

    if(token == tokenInfo.ETH.name) {
        var tokenEtherBalanceBigNumber = web3.eth.getBalance(config.account);
        return web3.fromWei(tokenEtherBalanceBigNumber.toNumber(), "ether" );
    }else {
        contractAbi = XPAabi;
        switch (token){
            case tokenInfo.XPA.name:
                tokenAddress = tokenInfo.XPA.address;
            case tokenInfo.ZRX.name:
                tokenAddress = tokenInfo.ZRX.address;   
            case tokenInfo.EOS.name:
                tokenAddress = tokenInfo.EOS.address;
            case tokenInfo.TRX.name:
                tokenAddress = tokenInfo.TRX.address;
            case tokenInfo.QTUM.name:
                tokenAddress = tokenInfo.QTUM.address;       
            case tokenInfo.OMG.name:
                tokenAddress = tokenInfo.OMG.address;
            case tokenInfo.BNB.name:
                tokenAddress = tokenInfo.BNB.address;   
            case tokenInfo.KNC.name:
                tokenAddress = tokenInfo.KNC.address;
            case tokenInfo.BNT.name:
                tokenAddress = tokenInfo.BNT.address;
            case tokenInfo.STORJ.name:
                tokenAddress = tokenInfo.STORJ.address;       
            case tokenInfo.RDN.name:
                tokenAddress = tokenInfo.RDN.address;
            case tokenInfo.ST.name:
                tokenAddress = tokenInfo.ST.address;   
            case tokenInfo.ICX.name:
                tokenAddress = tokenInfo.ICX.address;
            case tokenInfo.DGD.name:
                tokenAddress = tokenInfo.DGD.address;
            case tokenInfo.PPT.name:
                tokenAddress = tokenInfo.PPT.address;       
            case tokenInfo.MKR.name:
                tokenAddress = tokenInfo.MKR.address;
            case tokenInfo.SNT.name:
                tokenAddress = tokenInfo.SNT.address;   
            case tokenInfo.REP.name:
                tokenAddress = tokenInfo.REP.address;
            case tokenInfo.QASH.name:
                tokenAddress = tokenInfo.QASH.address;
            case tokenInfo.CVC.name:
                tokenAddress = tokenInfo.CVC.address;       
        }
    }
    
    let contract = web3.eth.contract(contractAbi).at(tokenAddress);
    let tokenBalanceBigNumber = contract.balanceOf(config.account);
    let tokenBalance = web3.fromWei(tokenBalanceBigNumber.toNumber(), "ether" );
    return tokenBalance;
}

export function checkTokenAddr(token) {
    switch(token){
        case tokenInfo.ETH.name:
            return tokenInfo.ETH.address.slice(2,);
        case tokenInfo.XPA.name:
            return tokenInfo.XPA.address.slice(2,);
        case tokenInfo.ZRX.name:
            return tokenInfo.ZRX.address.slice(2,);
        case tokenInfo.EOS.name:
            return tokenInfo.EOS.address.slice(2,);
        case tokenInfo.TRX.name:
            return tokenInfo.TRX.address.slice(2,);
        case tokenInfo.QTUM.name:
            return tokenInfo.QTUM.address.slice(2,);    
        case tokenInfo.OMG.name:
            return tokenInfo.OMG.address.slice(2,);
        case tokenInfo.BNB.name:
            return tokenInfo.BNB.address.slice(2,);
        case tokenInfo.KNC.name:
            return tokenInfo.KNC.address.slice(2,);
        case tokenInfo.BNT.name:
            return tokenInfo.BNT.address.slice(2,);
        case tokenInfo.STORJ.name:
            return tokenInfo.STORJ.address.slice(2,);      
        case tokenInfo.RDN.name:
            return tokenInfo.RDN.address.slice(2,);
        case tokenInfo.ST.name:
            return tokenInfo.ST.address.slice(2,); 
        case tokenInfo.ICX.name:
            return tokenInfo.ICX.address.slice(2,);
        case tokenInfo.DGD.name:
            return tokenInfo.DGD.address.slice(2,);
        case tokenInfo.PPT.name:
            return tokenInfo.PPT.address.slice(2,);  
        case tokenInfo.MKR.name:
            return tokenInfo.MKR.address.slice(2,);
        case tokenInfo.SNT.name:
            return tokenInfo.SNT.address.slice(2,);
        case tokenInfo.REP.name:
            return tokenInfo.REP.address.slice(2,);
        case tokenInfo.QASH.name:
            return tokenInfo.QASH.address.slice(2,);
        case tokenInfo.CVC.name:
            return tokenInfo.CVC.address.slice(2,);    
    }
}