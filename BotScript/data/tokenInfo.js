module.exports = {
    ETH: {
      name: 'ETH',
      address: '0x0',
    },
    XPA: {
      name: 'XPA',
      address: '0x958ae55ebba860583bd291aa1003ba07ddac8435'
    },
    ZRX: {
      name: 'ZRX',
      address: '0xdd26622deefa23cb3ac8c9c1041db50287b75bcf'
    },
    EOS: {
      name: 'EOS',
      address: '0x53e78733e19bb5e04bf8bb58b84a64451eb0d4e1'
    },
    TRX: {
      name: 'TRX',
      address: '0x666fbb2c7c2eb24ff1fc149d05ce33c8240f18da'
    },
    QTUM: {
      name: 'QTUM',
      address: '0x17007ee42635805a61ad946d4c947ce5f2b77faf'
    },
    OMG: {
      name: 'OMG',
      address: '0x9aecedd7b0f7d62d952b1f013ced721da4a4badd'
    },
    BNB: {
      name: 'BNB',
      address: '0x25c05ab3c27a04cb465a6840e680eeb745a4327a'
    },
    KNC: {
      name: 'KNC',
      address: '0xd54c6c182aeaff2db38c6fd31d49fd3d6272e89a'
    },
    BNT: {
      name: 'BNT',
      address: '0x4fd8668891e8425423c77a1e07acf8c6e3c43cde'
    },
    STORJ: {
      name: 'STORJ',
      address: '0x30002d1df50b90980a064e0c781087ac1368dd0d'
    },
    RDN: {
      name: 'RDN',
      address: '0x13f69bb6e699eab6d7bcdc05cad9b669e88e7992'
    },
    BNT: {
      name: 'BNT',
      address: '0x4fd8668891e8425423c77a1e07acf8c6e3c43cde'
    },
    ST: {
      name: 'ST',
      address: '0xaefc346692da078e360efcdf9936f9de195ed3d2'
    },
    BNT: {
      name: 'BNT',
      address: '0x4fd8668891e8425423c77a1e07acf8c6e3c43cde'
    },
    ICX: {
      name: 'ICX',
      address: '0xaf30d061ae60162a31e83cb987121e8e54823065'
    },
    DGD: {
      name: 'DGD',
      address: '0x1614255479c5ad329cb23b900da1ac1746b3e89e'
    },
    PPT: {
      name: 'PPT',
      address: '0x2682c052557436e46a2e0073b6c7974a3a511950'
    },
    MKR: {
      name: 'MKR',
      address: '0xdb8918abe02604ad2125f61c23bb22f0ea37879b'
    },
    SNT: {
      name: 'SNT',
      address: '0xc57bbda20ccef3a84bf9329736c67f403188c1fb'
    },
    REP: {
      name: 'REP',
      address: '0x8fac1fcbf065c5540cb54c4d034ef9d0adaa047f'
    },
    QASH: {
      name: 'QASH',
      address: '0x13f378711f7453eb19bf5f842f19dd1ce54bdfc1'
    },
    CVC: {
      name: 'CVC',
      address: '0x14f982b041dfc8719a2f5fc68915e7b5ffff57bd'
    },
}