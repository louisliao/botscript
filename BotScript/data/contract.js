
let balivAddress = process.argv;

if(balivAddress[2].length < 40) {
  throw "Please take Baliv address as first input !";
}

/* set the default accounts */
module.exports = {
  Ledger: "0x862dd9041bb0a0c92b3dadd27a9bc9986b6c46fc",
  Baliv: balivAddress[2],
  Rich: "0xa8b8057f02e1d21ccf5707a1d18747a3976c119a",
  Bank: "0x011585f544af1b6b3603770a7d4704df2197237a"
}