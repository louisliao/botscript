# Botscript 

## Setting testing account
In `/data/config.js`
setting your account address and privateKey.

## ETHtoXPAscript.sh
Doing ETH/XPA trade.

In shell script:
input parameter: babel-node test.js (baliv address) (fromToken) (toToken) (price) (amount)
For example:
```
babel-node test.js "0x42129362ebd6ccc48c7bab2946baf1c96b78900e" "ETH" "XPA" ${price[$i]} ${amount[$i]} &
```

## If error , Do approve 0
input parameter: babel-node test.js (baliv address) "approve0" (tokenName) 
For example:
```
babel-node test.js "0x42129362ebd6ccc48c7bab2946baf1c96b78900e" "approve0" "XPA"